<?php

namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;

class Hello extends Command
{
    private $serv;

    public function __construct() {
        $this->serv = new \swoole_server("0.0.0.0", 9501);
        $this->serv->set(array(
           'open_length_check' => true,
            'package_max_length' => 1024*1024*1024*2,
//            'package_length_type' => 'N',
//            'package_length_offset' => 0,
//            'package_body_offset' => 4,
            'worker_num' => 8,
            'daemonize' => false,
            'heartbeat_check_interval' => 30,
            'heartbeat_idle_time' => 30,
        ));

        $this->serv->on('Start', array($this, 'onStart'));
        $this->serv->on('Connect', array($this, 'onConnect'));
        $this->serv->on('Receive', array($this, 'onReceive'));
        $this->serv->on('Close', array($this, 'onClose'));

        $this->serv->start();
    }

    public function onStart( $serv ) {
        echo "Start\n";
    }

    public function onConnect( $serv, $fd, $from_id ) {
        $serv->send( $fd, "Hello {$fd}!" );
    }

    public function onReceive( \swoole_server $serv, $fd, $from_id, $data ) {
//        $data = bin2hex($data);
//        echo "Get Message From Client {$fd}:{$data}\n";
//        $serv->send($fd, "Server: ".$data);
//        echo $data;
        $index = new \app\index\controller\Index;
        $res = $index->transfer($data,$fd);
        echo ' ' . $res . " \r\n";
        $serv->send($fd, $res);
    }

    public function onClose( $serv, $fd, $from_id ) {
        echo "Client {$fd} close connection\n";
//        $b = 'abcd';
//        echo $b[1];
//        echo " ".('b' & 0xff)." \r\n ";
//        \think\Db::name('test')->insert(['data'=>$fd.'端口连接','add_time'=>date('Y-m-d H:i:s',time())]);
    }
}

$server = new \app\command\Hello;

