<?php
namespace app\index\controller;

class Index
{
    //中转方法
    public function transfer($data,$fd){
        $body['header'] = $header = bin2hex(substr($data,0,2));//报文头
        $body['message_len'] = $message_len = hexdec(bin2hex(substr($data, 21, 2)));//报文长度
        if($header == '5566' ) {
            //&& $message_len + 2 == strlen($data)
            $body['id'] = $id = substr($data, 2, 17);//ID号
            $body['frame_type'] = $frame_type = bin2hex(substr($data, 19, 1));//帧类型
            $body['message_type'] = $message_type = bin2hex(substr($data, 20, 1));//报文类型

            echo ' 报文头 ' . $header . "\r\n";
            echo ' ID号 ' . $id . "\r\n";
            echo ' 帧类型 ' . $frame_type . "\r\n";
            echo ' 报文类型 ' . $message_type . "\r\n";
            echo ' 报文长度 ' . $message_len . "\r\n";

            $res = ['status'=>-1,'data'=>[]];
            $result = false;
            switch ($message_type) {
                case '01':
                    switch ($frame_type) {
                        case '01':
                            var_dump(bin2hex($data));
                            //行波电流数据报文
                            $res = $this->travel($data,$body);
                            if($res['status']==1) {
                                $info = $res['data'];
                                $result =  $this->answer($info['id'], "02", $info['message_type'], '00');
                            }
                            break;


                        case '03':
                            //装置复位报文
                            $res = $this->restore($data,$body);
                            if($res['status']==1) {
                                $info = $res['data'];
                                $result =  $this->answer($info['id'], "04", $info['message_type'], '00');
                            }
                            break;


                        case '05':
                            //心跳数据报文
                            $res = $this->heartbeat($data,$body);
                            if($res['status']==1) {
                                $info = $res['data'];
                                $result =  $this->answer($info['id'], "06", $info['message_type'], '00');
                            }
                            break;


                        default:
                            //默认直接退出循环
                            break;
                    }
                    break;


                case '03':
                    switch ($frame_type) {
                        case '01':
                            //故障电流数据报文,与行波电流数据报文格式类似
                            $res = $this->travel($data,$body);
                            if($res['status']==1) {
                                $info = $res['data'];
                                $result =  $this->answer($info['id'], "02", $info['message_type'], '00');
                            }
                            break;


                        case '03':
                            //参数设置报文
                            $this->setParam($data,$body);

                            break;


                        case '05':
                            //基本信息数据报文
                            $res = $this->basicInfo($data,$body);
                            if($res['status']==1) {
                                $info = $res['data'];
                                $result =  $this->answer($info['id'], "06", $info['message_type'], '00');
                            }

                            break;


                        default:
                            break;
                    }
                    break;


                case '05':
                    switch ($frame_type) {
                        case '03':
                            //参数读取报文
                            $this->getParam($data,$body);
                            break;


                        case '05':
                            //工况数据报文
                            $res = $this->workCondition($data,$body);
                            if($res['status']==1) {
                                $info = $res['data'];
                                $result =  $this->answer($info['id'], "06", $info['message_type'], '00');
                            }
                            break;


                        default:
                            break;
                    }
                    break;


                case '07':
                    switch ($frame_type) {
                        case '05':
                            //装置故障信息报文
                            $res = $this->deviceMalfunction($data,$body);
                            if($res['status']==1) {
                                $info = $res['data'];
                                $result =  $this->answer($info['id'], "04", $info['message_type'], '00');
                            }
                            break;


                        default:
                            break;
                    }
                    break;

                default:
                    break;
            }
            return $result;
        }else{
            return false;
        }
    }

    //行波电流数据报文
    public function travel($data,$body=[]){
        var_dump(strlen($data));
        $body['package_length'] = substr($data,23,2);//数据包段长度
        $body['data'] = substr($data,25,1410);//波形数据
        var_dump($body['data']);
        $start_time = substr($data,1435,12);//波形起点的时间
        var_dump($start_time);
        $body['start_time'] = $this->byte2time($start_time);
        $body['data_length'] = substr($data,1447,2);//波形数据总长度
        $body['package_no'] = substr($data,1449,1);//数据包段号
        $body['package_num'] = substr($data,1450,1);//数据总包数
        $body['extra'] = substr($data,1451,0);//备用
        $body['verify'] = hexdec(bin2hex(substr($data,1451,2)));//校验位
        var_dump($body);
        return ['status'=>1,'data'=>$body];

    }

    //装置复位报文
    public function restore($data,$body=[]){
        $body['content'] = substr($data,23,0);
        $body['verify'] = hexdec(bin2hex(substr($data,23,2)));
        var_dump($body);
        return ['status'=>1,'data'=>$body];
    }

    //参数读取报文处理
    public function getParam($data,$body=[]){
        $body['content'] = substr($data,23,0);//报文内容
        $body['verify'] = hexdec(bin2hex(substr($data,23,2)));//校验位
        var_dump($body);
        return ['status'=>1,'data'=>$body];
    }

    //参数设置报文
    public function setParam($data,$body){
        //参数数量--从ASCII码转十六进制，再从十六进制转为十进制
        $body['param_num'] = hexdec(bin2hex(substr($data,23,2)));
        $start = 25;
        $param = [];
        for ($i = $body['param_num']; $i>=0; $i --){
            $no = hexdec(bin2hex(substr($data,$start,2)));
            $start += 2;
            $content = hexdec(bin2hex(substr($data,$start,4)));
            $start += 4;
            $param[$no] = $content;
        }
        var_dump($body);
        return ['status'=>1,'data'=>$body];
    }

    //心跳报文
    public function heartbeat($data,$body=[]){
        $body['verify'] = bin2hex(substr($data,131,2));//校验位
        var_dump($body);
        return ['status'=>1,'data'=>$body];
    }

    //基本信息数据报文
    public function basicInfo($data,$body=[]){
        $body['terminal_name'] =  substr($data,23,50);//终端名称
        $body['terminal_model'] = substr($data,73,10);//终端型号
        $body['terminal_version'] = substr($data,83,4);//终端基本信息版本号
        $body['manufacturers'] =  substr($data,87,50);//生产厂家
        $body['date'] = substr($data,137,4);//生产日期
        $body['no'] = substr($data,141,20);//出厂编号
        $body['extra'] = substr($data,161,30);//备用
        $body['verify'] = bin2hex(substr($data,131,2));//校验位
        var_dump($body);
        return ['status'=>1,'data'=>$body];
    }

    //工况数据报文
    public function workCondition($data,$body=[]){
        $upload_time = substr($data,23,12);
        $body['upload_time'] = $this->byte2time($upload_time);
        $body['power_supply_status'] = substr($data,35,1);//电池供电状态
        $body['voltage'] = substr($data,36,2);//电池电压
        $body['device_temp'] = substr($data,38,2);//设备温度
        $body['electricity_value'] = substr($data,40,2);//电流有效值
        $body['extra'] = substr($data,42,30);//备用
        $body['verify'] = hexdec(bin2hex(substr($data,72,2)));//校验位
        var_dump($body);
        return ['status'=>1,'data'=>$body];
    }

    //装置故障信息报文
    public function deviceMalfunction($data,$body=[]){
        $time = substr($data,23,12);//装置故障信息采集时间
        $body['acquisition_time'] = $this->byte2time($time);
        $len = $body['message_len'] - 35;
        $body['device_info'] = substr($data,35,$len);
        $body['verify'] = hexdec(bin2hex(substr($data,$body['message_len'],2)));
        var_dump($body);
        return ['status'=>1,'data'=>$body];
    }





    //byte转换成时间
    public function byte2time($str) {
        var_dump(bin2hex($str));
        var_dump($str);
        $bytes = [];
        for($i=0; $i <strlen($str); $i++){
            $bytes[] = hexdec(bin2hex(ord($str[$i])));
        }
        var_dump($bytes);
        $time['year'] = $bytes[0] ;
        $time['month'] = $bytes[1] + 1;
        $time['day'] = $bytes[2];
        $time['hour'] = $bytes[3];
        $time['minute'] = $bytes[4];
        $time['second'] = $bytes[5];
        $time['millisecond'] = (int)($bytes[6] . $bytes[7]);
        $time['microsecond'] = (int)($bytes[6] . $bytes[7]);
        $time['nanosecond'] = (int)($bytes[6] . $bytes[7]);
        return $time;
//        return $val;
    }

    //默认应答报文
    public function answer($id,$frame_type,$message_type,$status){
        $str = '5566';//添加报文头
        for ($i = 0; $i < strlen($id); $i++){
            $str .= bin2hex($id[$i]);
        }
        $str .= $frame_type;//添加帧类型
        $str .= $message_type;//添加报文类型

//        var_dump(str_pad(dechex(strlen($str)/2),4,"0",STR_PAD_LEFT));
//        $str .= str_pad(dechex(strlen($str)/2),4,"0",STR_PAD_LEFT);//添加报文长度
        $str .= '0018';
        $str .= $status;//添加报文状态
        $content = 0;
        //计算校验码
        for ($i=0; $i<(strlen($str)/2); $i++ ){
            $content += hexdec(substr($str,2*$i,2));
        }
        $str .= str_pad(dechex($content),4,"0",STR_PAD_LEFT);//添加校验位
        //将每两位十六进制数转换成ascii码
//        $string = '';
        for ($i=0; $i<(strlen($str)/2); $i++ ){
            echo ' '.substr($str,2*$i,2).' -> '.hex2bin(substr($str,2*$i,2)) ."\r\n";
            if($i == 0){
                $string = hex2bin(substr($str,2*$i,2));
            }else{
                $string .= hex2bin(substr($str,2*$i,2));
            }

        }
        return $string;
    }

    //根据byte和装置参数表获取信息
    public function getByte($param){

    }


}
