<?php



class RedisEx
{
    /*定义一个*/
    private $redis;

    //redis的服务列表
    private static $serverlist=array();

    //自动加载配置项

    public function __construct($config=array())
    {
        if(empty($config)){
            $config=array(
                "host"=>"127.0.0.1",//地址
                "port"=>"6379",//端口
                "timeOut"=>"10",//超时时间
                "password"=>"",//密码
            );
        }
        $this->inirRedis($config);
    }

    /*连接*/
    function inirRedis($config){
        if(array_key_exists($config["host"],self::$serverlist)){
            return $this->redis;
        }
        $this->redis = new redis();
        //连接redis
        $this->redis->connect($config["host"],$config["port"],$config["timeOut"]);
        //密码验证
        if(isset($config["password"])&& $config["password"]){
            $this->redis->auth($config["password"]);
        }
        //判断是否链接超时
        if(strcasecmp($this->redis->ping(),"+PONG")!=0){
            $this->redis=null;
        }
        if($this->redis){
            self::$serverlist[$config["host"]]=true;
        }
        if($this->redis==null){
            die("redis连接异常！！！");
        }
    }

    /**获取内容
     * @param
     */
    function get($key){
        return  $this->redis->get($key);
    }

    /**
     * 设置字符串
     * $key 键名
     * $timeout 生存时间
     * $value 键值
     */
    function setstring($key,$timeout,$value){
        if($timeout>0){
            $this->setex($key,$timeout,$value);
        }else{
            $this->redis->set($key,$value);
        }
    }

    /**
     * 防止覆盖原来的数据
     */
    function setnx($key,$val){
        $res=$this->redis->SETNX($key,$val);
        if($res==1){
            return array("code"=>200,"新增成功");
        }else{
            return array("code"=>0,"新增失败,内容已存在");
        }
    }

    /**
     * 删除内容
     */
    function delete($key){
        $res=$this->redis->del($key);
        if($res==1){
            return array("cdoe"=>"200","msg"=>"删除成功");
        }else{
            return array("cdoe"=>"-200","msg"=>"删除失败");
        }
    }

    /**
     * 增加数量
     * $key
     * $num 默认是1
     */
    function setinc($key,$num=1){
        return  $this->redis->INCRBYFLOAT($key,$num);
    }

    /**
     * 减少数量
     */
    function setdec($key,$num){
        return  $this->redis->DECRBY($key,$num);
    }


    /*--------------------------------------------------------------队列操作---------------------------------------------------队列操作-------------------------------------------------*/
    /**
     * 设置队列(左进右出)
     * @param $key 队列名称
     * @param $val 队列的数据
     */
    function setleftlist($key,$val){
        if(is_array($val)){
            if(!empty($val)){
                foreach ($val as $v){
                    $this->redis->lpush($key,$v);
                }
            }else{
                die("数组为空");
            }
        }else{
            return $this->redis->lpush($key,$val);
        }
    }

    /**
     * 读取队列(从右边读取和setleftlist向对应)
     * $key 键名
     */
    function popright($key){
        return $this->redis->rpop($key);
    }

    /**
     * 设置队列(右进左出)
     * @param $key
     * @param $val
     */
    function setrightlist($key,$val){
        if(is_array($val)){
            if(!empty($val)){
                foreach ($val as $v){
                    $this->redis->rpush($key,$v);
                }
            }else{
                die("数组为空");
            }
        }else{
            return $this->redis->rpush($key,$val);
        }
    }

    /**
     * 读取队列(从左侧取出内容)
     */
    function popleft($key){
        return $this->redis->lpop($key);
    }

    /**
     * 获取队列的个数
     */
    function getlistcount($key){
        return $this->redis->llen($key);
    }

    /**
     * 获取队列区间的内容
     * $key 键名
     * $start 开始位置 默认为0
     * $end 结束位置 默认为-1
     */
    function getlistlrange($key,$start=0,$end=-1){
        return $this->redis->lrange($key,$start,$end);
    }


    /*--------------------------------------------------------------hash操作---------------------------------------------------hash操作--------------------------------------------*/
    /**
     * 设置hash缓存
     * 对象的存储
     *一维数组，键值对格式，如：array('key1' => 'item1','key2' => 'item2')
     */
    function sethash($key,$val){
        $this->redis->hmset($key,$val);
    }

    /**
     * hsetnx 存在的字段不允许修改返回0，不存在的字段可以添加 ,添加成功返回1
     * $team hash表名
     * $arr 要新增的键值对
     */
    function sethsetnx($team,$arr){
        if(!is_array($arr)|| empty($arr)){
            return array("code"=>"-200","msg"=>"格式不对或者数据为空");
        }
        foreach ($arr as $k=>$v){
            $this->redis->hsetnx($team,$k,$v);
        }
    }
    /**
     * hset 创建或者修改1个参数
     * 不存在新增,
     * 存在修改
     * $key hash表名
     * $val 键值对
     */
    function sethset($key,$val){
        if(!is_array($val)|| empty($val)){
            return array("code"=>"-200","msg"=>"格式不对或者数据为空");
        }
        foreach($val as $k=>$v){
            $this->redis->hset($key,$k,$v);
        }
    }

    /**获取hash缓存
     * @param $key 名称
     * @param null $hashKey 默认为空，为空显示全部，否则显示单个
     */
    function gethash($key,$hashKey=null){
        if($hashKey){
            //如果不为空显示指定的数据
            return is_string($hashKey)?$this->redis->hget($key):$this->redis->hmet($key,$hashKey);
        }
        return $this->redis->hGetAll($key);
    }

    /**
     * 增加/减少数量。支持浮点，正数，负数
     * 返回修改后的值
     */
    function hashdecorinc($item,$key,$val){
        return $this->redis->HINCRBYFLOAT($item,$key,$val);
    }

    /**
     *删除hash
     * $key 名
     * $item 内容支持一维数组或者字符串
     */
    function removehash($key,$item){
        if(is_array($item)){
            $keys=array_keys($item,"",true);
            $string=implode("",$keys);
            $res=$this->redis->hdel($key,$string);
        }else{
            $res=$this->redis->hdel($key,$item);
        }
        if($res>0){
            return array("cdoe"=>"200","msg"=>"删除成功");
        }else{
            return array("cdoe"=>"-200","msg"=>"删除失败");
        }
    }


    /*--------------------------------------------------------------无序集合操作---------------------------------------------------无序集合操作--------------------------------------------*/

    //集合成员是唯一的，这就意味着集合中不能出现重复的数据。可以做菜谱，参赛人名单等。
    //set命令制作反垃圾系统。先创建一个带有垃圾内容的集合，再新建一个正常的内容集合。利用差集，显示正常集合里正常的集合

    /**
     * 添加集合数据
     * 支持数组,字符串
     */
    function setCollection($key,$arr){
        if(is_array($arr)){
            foreach($arr as $v){
                $this->redis->sadd($key,$v);
            }
        }else{
            $this->redis->sadd($key,$arr);
        }
    }

    /**
     *获取set无序集合
     *
     */
    public function getCollection($key) {
        return $this->redis->sMembers($key);
    }

    /**
     * 删除set无序集合中的元素
     * @param $key 集合key名称
     * @param $items 待删除的元素，支持单个数据和一维数组
     */
    public function removeCollection($key,$items) {
        if (is_array($items)) {
            foreach ($items as $item) {
                $this->redis->sRem($key,$item);
            }
        } else {
            $this->redis->sRem($key,$items);
        }
    }

    /**
     * 检查set无序集合中是否包含某个元素
     * @param $key 集合key名称
     * @param $item 元素
     * @return mixed true 包含 | false 不包含
     */
    public function collectionExists($key,$item) {
        return $this->redis->sIsMember($key, $item);
    }

    /**
     * 获取set无序集合中元素的个数
     * @param $key 集合key名称
     * @return mixed
     */
    public function getCollectionCount($key) {
        return $this->redis->sCard($key);
    }

    /**
     * 获取两个set无序集合的交集
     * @param $key1
     * @param $key2
     * @return mixed
     */
    public function getCollectionInter($key1,$key2) {
        return $this->redis->sInter($key1, $key2);
    }

    /**
     * 获取两个set无序集合的并集
     * @param $key1
     * @param $key2
     * @return mixed
     */
    public function getCollectionUnion($key1,$key2) {
        return $this->redis->sUnion($key1, $key2);
    }

    /**
     * 获取两个set无序集合的差集
     * @param $key1
     * @param $key2
     * @return mixed
     */
    public function getCollectionDiff($key1,$key2) {
        return $this->redis->sDiff($key1, $key2);
    }



    /*--------------------------------------------------------------有序集合操作---------------------------------------------------有序集合操作--------------------------------------------*/
    //排行
    /**
     * 设置set有序集合
     * @param $key 集合key名称
     * @param $items 集合数据，格式：array('score1' => 'item1','score2' => 'item2')
     */
    public function setZCollection($key,$items) {
        foreach ($items as $score => $item) {
            $this->redis->zAdd($key, $score, $item);
        }
    }
    /**
     * 获取set有序集合
     * 应用场景：排行榜
     * @param $key 集合key名称
     * @param string $sort 排序方式，asc|desc，默认为增序
     * @param int $start 开始位置，默认为集合第一个元素
     * @param int $end 结束位置，默认为集合最后一个元素
     * @return mixed
     */
    public function getZCollection($key,$sort='asc',$start=0,$end=-1) {
        if ($sort == 'asc') {
            // 成员按分数值递增排序，分数值相同的则按元素字典序来排序
            return $this->redis->zRange($key, $start, $end);
        }
        // 成员按分数值递减排序，分数值相同的则按元素字典序的逆序来排序
        return $this->redis->zRevRange($key, $start, $end);
    }

    /**
     * 删除set有序集合中的元素
     * @param $key 集合key名称
     * @param $items 待删除的元素，支持单个数据和一维数组
     */
    public function removeZCollection($key,$items)
    {
        if (is_array($items)) {
            foreach ($items as $item) {
                $this->redis->zRem($key, $item);
            }
        } else {
            $this->redis->zRem($key, $items);
        }
    }

    /**
     * 增加set有序集合中指定元素的score
     * 应用场景：计数器、统计点击数等
     * @param $key 集合key名称
     * @param $item 元素
     * @param int $num 增量，默认为1
     */
    public function zCollectionIncr($key,$item,$num=1) {
        $this->redis->zIncrBy($key, $num, $item);
    }

    /**
     * 获取set有序集合中元素的个数
     * @param $key 集合key名称
     * @return mixed
     */
    public function getZCollectionCount($key) {
        return $this->redis->zCard($key);
    }

    /**
     * 检查set有序集合中是否包含某个元素
     * @param $key 集合key名称
     * @param $item 元素
     * @return bool true 包含 | false 不包含
     */
    public function zCollectionExists($key,$item) {
        if ($this->redis->zScore($key, $item) !== false) {
            return true;
        }
        return false;
    }

}
